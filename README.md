# Postgres Migrations

This system provides a way of managing database migrations using
Postgres itself. It creates a subschema called `migrations`, and under
this stores hashed migration descriptions, migration dependencies, and
which have been applied.

## Migrations

A _migration_ is a stored procedure read from a file or inserted from
Haskell code as part of a call to the `add_migration` function in
Postgres. The function should take no parameters, and return `0` on
success, and anything else on failure.

The corresponding Haskell function is `add_migration :: Migration ->

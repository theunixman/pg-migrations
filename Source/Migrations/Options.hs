{-|
Module:             Options
Description:        Option parsing for postgres connection.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Options (execParser, commandsInfo, MigrationCommands(..)) where

import Lawless hiding (on, argument)
import Path

import Options.Applicative
import Options.Applicative.Types
import Network
import Data.ByteString.Char8 hiding (empty)
import Database.PostgreSQL.Migrations

-- * Connection Parameters

-- ** IP Connection

-- | 'HostName' of the database
hostName ∷ Parser HostName
hostName = strOption (long "host" <> short 'h' <> metavar "HOSTNAME" <> help "database host name")

-- | 'PortNumber' of the database
portNumber ∷ Parser PortID
portNumber =
    option (PortNumber ∘ fromInteger <$> auto) (long "port"
                                                <> short 'p'
                                                <> metavar "PORT"
                                                <> help "database port number")

-- ** UNIX Socket connection
unixSocket ∷ Parser PortID
unixSocket =
    let
        p ∷ [Char] → Either [Char] AbsFile
        p = parse
        v ∷ ReadM PortID
        v = readerAsk
            >>= \o → case p o of
                         Left e → readerError ("Could not parse socket as absolute file: " <> e)
                         Right s → return ∘ UnixSocket ∘ toString $ s
    in
        option v (long "socket"
                                <> short 's'
                                <> metavar "UNIX-SOCKET"
                                <> help "Unix Socket path (absolute)")

data Connection =
    IPConnection {
    ipcHostName ∷ HostName,
    ipcPort ∷ PortID
    } |
    SocketConnection PortID
    deriving (Eq, Show)

connection ∷ Parser Connection
connection = (IPConnection <$> hostName <*> portNumber) <|> (SocketConnection <$> unixSocket)

-- ** Common database and user name parameters
byteStringM ∷ ReadM ByteString
byteStringM = pack <$> readerAsk

database ∷ Parser ByteString
database = option byteStringM (long "database"
                               <> short 'd'
                               <> metavar "DATABASE"
                               <> help "Database name")

user ∷ Parser ByteString
user = option byteStringM (long "user"
                           <> short 'u'
                           <> metavar "USER"
                           <> help "User name")

-- ** Database connection parser and info

dbConnection ∷ Parser PGDatabase
dbConnection =
    let
        d (IPConnection{..}) = defaultPGDatabase {pgDBHost = ipcHostName, pgDBPort = ipcPort}
        d (SocketConnection s) = defaultPGDatabase {pgDBPort = s}
        db c b u = (d c) {pgDBName = b, pgDBUser = u}
    in
        db <$> connection <*> database <*> user

-- * Commands

data MigrationCommands =
    Initialize {initDBConn ∷ PGDatabase}
    deriving (Eq)

commands ∷ Parser MigrationCommands
commands = subparser (
    command "initialize" (info (Initialize <$> dbConnection) (
                                 progDesc "Initialize the migrations framework")
                         )
    )

commandsInfo ∷ ParserInfo MigrationCommands
commandsInfo = info commands (
    fullDesc
    <> header "Postgres Migration Tools"
    <> progDesc "Manages current and new database migrations."
    )

module Main where

import Lawless
import IO
import Options
import Database.PostgreSQL.Migrations

main :: IO ()
main = execParser commandsInfo
       >>= \(Initialize db)→ runManaged $ do
    conn ← connect db
    liftIO $ initializeMigrations conn

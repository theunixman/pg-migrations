-- The dependency graph between migrations. This table refers back to
-- migrations.migrations. A record must be here for a migration to be
-- included. The key is the tuple of the migration and the migration
-- it depends on. We don't allow circular references.
create table migrations.dependency (
       migration migration_id not null references migration(migration),
       depends_on migration_id not null references migration(migration) check (depends_on != migration),
       created_at timestamp with time zone not null default current_timestamp,

       primary key (migration, depends_on)
);

create index migrations_dependencies_depends_on_idx on dependency (depends_on);
create index migrations_dependencies_created_at on dependency (created_at desc);

-- The graph of migration dependencies
create or replace view migrations.graph as
       with recursive graph(succ, pred) as (
            select m.migration as succ, d.migration as pred
            from migration m left join dependency d on d.migration = m.migration
            where d.migration is null
       union all
            select d.migration as succ, m.migration as pred
            from migration m inner join dependency d on m.migration = d.depends_on
       )
       select * from graph;

-- The set of all migrations.
create table migrations.migration (
       migration migration_id not null primary key check ((migration).nbr > 0),
       content migration_text check ((content).body is not null),
       created_at timestamp with time zone not null default current_timestamp
);

create index migrations_migration_idx on migration (migration);
create index migrations_created_at on migration (created_at desc);

-- Ensure that we populate the hash of the  migration_text field.
create function check_create_migration() returns trigger as $check_create_migration$
begin
        if TG_OP != 'INSERT' then
           raise exception '%:% only INSERTS permitted on migration table.',
                 (NEW.migration).nbr, (NEW.migration).name;
        end if;

        NEW.content := text_hash(NEW.content);
        return NEW;
end;
$check_create_migration$ language plpgsql;

create trigger check_create_migration before insert or update or delete on migration
       for each row execute procedure check_create_migration();

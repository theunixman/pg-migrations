-- Create a schema for all the migrations machinery.
create schema migrations;

set search_path to migrations, public;

-- Load the crypto extensions if they're not already loaded.
create extension if not exists pgcrypto;

-- The key type for a migration
create type migrations.migration_id as (
       nbr bigint,
       name varchar(512),
       proc varchar(520)
);

-- The text and hash of the migration
create type migrations.migration_text as (
       body bytea,
       hash bytea
);

create function migrations.text_hash(mt migration_text) returns migration_text as $text_hash$
        select (mt.body), digest((mt).body, 'sha512' :: text)
$text_hash$ language sql strict immutable;

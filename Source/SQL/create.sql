-- Create a root migration.
create or replace function migrations.add_migration(m migration_id, b bytea)
returns bigint
as $function$
begin
        insert into migration (migration, content) values (m, row(b, null));
        return 0;
end;
$function$ language plpgsql;

-- Create a dependent migration.
create or replace function migrations.add_migration(migration migration_id, body bytea,
                                                                                         depends_on migration_id)
returns bigint
as $add_migration$
begin
        perform add_migration(migration, body);
        insert into dependency (migration, depends_on) values (migration, depends_on);
        return 0;
end;
$add_migration$ language plpgsql;

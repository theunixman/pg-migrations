-- The timestamp when a migration was applied.
create table migrations.applied (
       migration migration_id primary key references migration(migration),
       applied_at timestamp with time zone not null default current_timestamp
);

-- The graph of migrations that have been applied.
create or replace view migrations.applied_migrations as
       select g.succ, g.pred, a.applied_at
       from graph g inner join applied a on g.succ = a.migration;

-- The graph of migrations that have not been applied.
create or replace view migrations.unapplied_migrations as
       select g.succ, g.pred
       from graph g left join applied a on g.succ = a.migration
       where a.migration is null;

create function migrations.run_migrations_to(target migration_id)
       returns bigint
       called on null input
as $run_migrations_to$
declare mig record;
begin
        for mig in select * from unapplied_migrations loop
            if target is not null and target = mig then
                return 0;
            end if;
            raise notice 'Applying migration %:%', (mig.migration).nbr, (mig.migration).name;
            execute format('select  migrations.apply_%s_%s();',
                                    trim(from to_char((mig.migration).nbr, 'fm00000')),
                                    (mig.migration).name);
            update migrations.applied set applied_at = now() where migration = mig.migration;
       end loop;
       return 0;
end;
$run_migrations_to$ language plpgsql;

{-|
Module:             Database.PostgreSQL.Typed.Lifted
Description:        Lifted PostgreSQL Typed
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Database.PostgreSQL.Typed.Lifted (
    module Database.PostgreSQL.Typed,
    module Database.PostgreSQL.Typed.Query,
    module Database.PostgreSQL.Typed.Protocol,
    RawStatement,
    rawStatement,
    pgQuery,
    transaction,
    pgSimpleQueries_,
    connect
    ) where

import Lawless
import IO
import Exception

import Database.PostgreSQL.Typed (
    pgSQL,
    PGDatabase(..),
    defaultPGDatabase)
import Database.PostgreSQL.Typed.Query (
    PGSimpleQuery,
    makePGQuery,
    QueryFlags(..),
    simpleQueryFlags)
import Database.PostgreSQL.Typed.Protocol (PGConnection)

import qualified Database.PostgreSQL.Typed as T
import qualified Database.PostgreSQL.Typed.Query as Q
import qualified Database.PostgreSQL.Typed.Protocol as P
import Data.ByteString.Lazy.Char8 (ByteString, pack, unpack)

newtype RawStatement = RawStatement {unRawStatement ∷ ByteString} deriving (Eq, Ord, Show, Monoid)
rawStatement ∷ Iso' RawStatement [Char]
rawStatement = iso (unpack ∘ unRawStatement) (RawStatement ∘ pack)

pgQuery ∷ (MonadIO m, Q.PGQuery q a) ⇒ PGConnection -> q -> m [a]
pgQuery c q = liftIO $ Q.pgQuery c q

transaction ∷ (MonadMask m, MonadIO m) ⇒ PGConnection → (PGConnection → m a) → m a
transaction conn f = do
    let beg = liftIO $ P.pgBegin conn >> return conn
    let com = liftIO ∘ P.pgCommit
    let abrt = liftIO ∘ P.pgRollback
    let cerr = bracketOnError beg abrt f

    r ← cerr
    com conn
    return r

pgSimpleQueries_ ∷ (MonadIO m) ⇒ PGConnection → RawStatement → m ()
pgSimpleQueries_ conn s = liftIO $ P.pgSimpleQueries_ conn (unRawStatement s)

connect ∷ PGDatabase → Managed PGConnection
connect db = managed ((bracket (liftIO ∘ T.pgConnect $ db)) (liftIO ∘ T.pgDisconnect))

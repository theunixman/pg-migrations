{-|
Module:             Database.PostgreSQL.Migrations.Create
Description:        Create a new migration from Text or a file.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Database.PostgreSQL.Migrations.Create where

import Lawless
import Database.PostgreSQL.Typed

addMigration = [pgSQL|select migrations.add_migration()|]

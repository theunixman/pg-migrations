{-|
Module:             Database.PostgreSQL.Migrations.Initialize
Description:        Initialize the migrations framework schema.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Database.PostgreSQL.Migrations.Initialize (
    module Database.PostgreSQL.Migrations.Initialize.SQL
    ) where

import Database.PostgreSQL.Migrations.Initialize.SQL

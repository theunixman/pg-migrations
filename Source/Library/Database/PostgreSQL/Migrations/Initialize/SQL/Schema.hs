{-# Language TemplateHaskell, QuasiQuotes #-}

{-|
Module:             Database.PostgreSQL.Migrations.Initialize.SQL.Schema
Description:        SQL for creating the Migrations Schema and data types.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Database.PostgreSQL.Migrations.Initialize.SQL.Schema (statements) where

import Lawless
import Data.String.Here.Uninterpolated
import Database.PostgreSQL.Typed.Lifted (RawStatement, rawStatement)

type FileContents = [Char]

schema ∷ FileContents
schema = [hereFile|Source/SQL/schema.sql|]

migrations ∷ FileContents
migrations = [hereFile|Source/SQL/migrations.sql|]

dependencies ∷ FileContents
dependencies = [hereFile|Source/SQL/dependencies.sql|]

create ∷ FileContents
create = [hereFile|Source/SQL/create.sql|]

apply ∷ FileContents
apply = [hereFile|Source/SQL/apply.sql|]

statements ∷ RawStatement
statements = foldMapOf folded (review rawStatement) [schema, migrations, dependencies, create, apply]

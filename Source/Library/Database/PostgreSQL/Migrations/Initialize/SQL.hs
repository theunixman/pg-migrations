{-# Language TemplateHaskell, QuasiQuotes #-}

{-|
Module:             Database.PostgreSQL.Migrations.Initialize.SQL
Description:        Include SQL migration framework files in template haskell.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Database.PostgreSQL.Migrations.Initialize.SQL (initializeMigrations) where

import Lawless hiding (on)
import IO
import Exception
import Database.PostgreSQL.Typed.Lifted
import Database.PostgreSQL.Migrations.Initialize.SQL.Schema

migrationsExistQ ∷ PGSimpleQuery Bool
migrationsExistQ = [pgSQL|!
                         select exists (select table_schema, table_name from information_schema.tables
                         where table_schema = 'migrations' and table_name = 'migration')|]

isInitialized ∷ (MonadIO m) ⇒ PGConnection → m Bool
isInitialized conn = pgQuery conn migrationsExistQ >>= return ∘ fromMaybe False ∘ firstOf traversed

initializeMigrations ∷ (MonadMask m, MonadIO m) ⇒ PGConnection → m ()
initializeMigrations conn =
    unlessM (isInitialized conn) $ transaction conn $ flip pgSimpleQueries_ statements
